/**
* @abstract SQL migrate native program library functions
* @author Webauro
* @copyright GNU/GPL
*/

/*
* GNU General Public License Statement
* This file is part of SQL migrate native program.

* SQL migrate is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* SQL migrate is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with SQL migrate.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void print_license() {
	printf("\nSQL-migrate native program Copyright (C) 2013 Webauro\n");
    	printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
    	printf("This is free software, and you are welcome to redistribute it\n");
	printf("under certain conditions.\n");
}

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0'; // End string with NULL terminator
			break;
		}
	}
}

int log_err(const char *err) {
	char filename[] = "err_log";
	FILE *fp;
	if(file_exists(filename)) {
		fp = fopen(filename, "a+");
		fprintf(fp, "%s\n", err);
		fclose(fp);
	} else return 0;
	return 1;
}

int file_exists(char *filename) {
	int exists = 0;
	FILE *fp = fopen(filename, "r");
	if(fp) {
		exists = 1;
		fclose(fp);
	}
	return exists;
}



