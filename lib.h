/**
* @abstract SQL migrate native program header file
* @author Webauro
* @copyright GNU/GPL
*/

/*
* GNU General Public License Statement
* This file is part of SQL migrate native program.

* SQL migrate native program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* SQL migrate native program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with SQL migrate native program.  If not, see <http://www.gnu.org/licenses/>.
*/

void print_license();

void strip_newline(char *str, int len);

int log_err(const char *err);

int file_exists(char *filename);

