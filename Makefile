# @abstract Makefile SQLMigrate native program
# @author VizCreations
# @copyright GNU/GPL
#

#
# GNU General Public License Statement
# This file is part of SQL migrate native program.

# SQL migrate is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SQL migrate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SQL migrate.  If not, see <http://www.gnu.org/licenses/>.
#

# Makefile

SRC_CIL = lib.c
OBJ_CIL = lib.o

CIL_INCLUDES = -I/usr/local/mysql/include -I. -I/usr/local/include
CIL_LIBS = -L/usr/local/mysql/lib -lmysqlclient -L/usr/lib

all: lib_cil compile install

lib_cil:
	gcc -c $(SRC_CIL)
	ar rcs lib.a $(OBJ_CIL)
	$(RM) *.o

compile:
	gcc -o sqlmigrate $(CIL_INCLUDES) $(CIL_LIBS) main.c lib.a -lm -lc

install:
	cp sqlmigrate /usr/local/bin/

clean:
	rm sqlmigrate* *~
