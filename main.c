/**
* @abstract SQL migrate native program
* @author VizCreations
* @copyright GNU/GPL
*/

/*
* GNU General Public License Statement
* This file is part of SQL migrate native program.

* SQL migrate is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* SQL migrate is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with SQL migrate.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mysql.h>
#include "lib.h"

#define QUERIES "./sql.txt"
#define TRUE 1
#define FALSE 0
#define MAX_LINE 255
#define MAX_LINES 50
#define MAX_ROW 1000
#define MAX_ROWS 10000

/** Define MYSQL credentials */
#define MYSQLHOST "localhost"
#define MYSQLUSER "mysqlusername"
#define MYSQLPASS "mysqlpassword"
#define MYSQLSCHEMA "mysqlschemaname"

char buf[] = "";
int connected;

MYSQL *get_db_instance() { // Create instance (get conn)
	MYSQL *conn = NULL;
	MYSQL *con = NULL;
	connected = FALSE;
	conn = mysql_init((MYSQL *) 0);
	mysql_options(conn, MYSQL_READ_DEFAULT_GROUP, "mysqlcapi");
	con = mysql_real_connect(conn, MYSQLHOST, MYSQLUSER, MYSQLPASS, MYSQLSCHEMA, 0, NULL, 0);
	if(con == NULL)
		connected = FALSE;
	else
		connected = TRUE;
	return conn;
}

int close_db_instance(MYSQL *conn) {
	if(connected == TRUE) {
		mysql_close(conn);
	} else return FALSE;
	return TRUE;
}

int execute_db_query(MYSQL *conn, char *SQL) {
	char err[MAX_LINE];
	MYSQL_RES *result = NULL;
	//printf("Executing query:\t");
	//printf("%s\n", SQL);
	if(conn == NULL) printf("Technical problem!\n");
	int rc = mysql_query(conn, SQL);
	if(rc != 0) { // rc is zero when it's successful
		//printf("Failed!!! <check log>\n");
		sprintf(err, "%s", mysql_error(conn)); // mysql_error() always points to a character string at a single place in memory
		log_err(err);
	} else {
		//printf("Success!!!\n");
	}
	return rc;
}

MYSQL_ROW get_db_row(MYSQL *conn, int rc) {
	MYSQL_RES *result;
	MYSQL_ROW row;
	result = mysql_use_result(conn);
	row = mysql_fetch_row(result);
	mysql_free_result(result);
	return row;
}

char **get_db_rows(MYSQL *conn, int rc) {
	MYSQL_RES *result = NULL;
	MYSQL_ROW row;
	int count = 0;
	int rowscount = 0;
	int fieldscount = 0;
	int rowkey = 0;
	int fldkey = 0;
	int r = 0;
	char **results = (char **) malloc(sizeof(char **) * MAX_ROWS);
	result = mysql_use_result(conn);
	rowscount = mysql_num_rows(result);
	if(rowscount > 0) {
		fieldscount = mysql_num_fields(result);
		while((row=mysql_fetch_row(result)) != NULL) {
			//results[dimone] = (char *) malloc(sizeof(char *) * MAX_ROW);
			fldkey = 0;
			for(r=0; r<fieldscount; r++) {
				results[rowkey] = (char *) malloc(sizeof(char **) * MAX_ROW);
				strcpy(results[rowkey], row[r]);
				++fldkey;
			}
			++count;
			++rowkey;
		}
	}
	mysql_free_result(result); // Important to free memory/resource to system
	return results;
}

int main(int argc, char *argv[], char *env[]) {
	FILE *fp;
	char line[MAX_LINE];
	char *token;
	int linecount = 0;
	char host[20], user[20], pass[20], schema[20];
	char SQL[1024];
	int rc;
	int qkey = 1;
	MYSQL *conn = NULL;
	MYSQL *con = NULL;
	connected = FALSE;

	print_license();

	if(argc < 2) {
		printf("\nUsage: %s <queries text file>\n", argv[0]);
		return -1;
	}

	if(file_exists(argv[1])) {
		conn = get_db_instance();
		//strcpy(buf, "Hello world!");
		if(connected == FALSE) printf("Error connecting: %s\n", mysql_error(conn));
		else {
			//printf("Connected\n");
			sleep(1);
			fp = fopen(argv[1], "r");
			if(fp) {
				printf("Executing queries..\n");
				while(fgets(SQL, MAX_LINE, fp)) {
					strip_newline(SQL, MAX_LINE);
					printf("Query %d:\t", qkey);
					if(execute_db_query(conn, SQL) == 0) {
						printf("Success!!!\n");
					} else {
						printf("Failed!!! <check log>\n");
					}
					++qkey;
					sleep(2);
				}
				fclose(fp);
				printf("Exiting now..\n");
				sleep(1);
			} else {
				printf("Technical problem!\n");
				log_err("Tech error!!");
			}
		}
		close_db_instance(conn);
	} else {
		printf("\nQUERIES file doesn't exist! Exiting now..\n");
		return -1;
	}
	return 0;
}

