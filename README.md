SQL-migrate native program version1.0:
=====================================
SQL-migrate native program For Unix like systems is a very 
simple application to migrate database changes on the fly.
This binary runs natively on the operating system and doesn't need
another software to execute except of course your MySQL program.


OpenSource and upgrading the application:
=========================================
SQL migrate native is open source and has GPL v3 license attached to it.
This means that you can change the program to your own name, but still
give it away for FREE.


How to use:
===========
After you install the software to your system, run the following command 
to migrate/update your SQL queries present in the dummy sqlqueries.txt file.
Also, for your configuration, you have to edit/create a config.txt file 
which is included with the program. Change it accordingly to suit your settings.

> sqlmigrate sqlqueries.txt


Requirements:
=============
1. Unix/Linux/BSD or any other Unix like system
2. MySQL application from Oracle.com or Mysql.com
3. MySQL must be running in order to use SQL migrate


How to compile/install:
=======================
This is the tricky part. I assume you're a good Linux pro, and know
how to install from source. Otherwise, better get a friend who knows.
When you download this package and extract it to a folder, you have
to run the 'make all' command which will compile and install.

> make all

> make clean

> make compile

> make install

All these commands are valid in the Makefile (Remember you can change it) :)
If you want to clear your application data, just run 

> make reset


Source code and headers:
========================
The program is segregated into three files.
main.c
lib.c
lib.h

Although you see some additional files, just forget about them,
and I don't have to tell you about them, if you're a C programmer.
The structures are declared and defined in the header file, whereas
the functions are only declared in it. Functions are defined in the
lib.c source file. The main.c uses MYSQL structs and functions directly.


Feedback:
=========
The project is open for anyone to change and post back.
Happy programming,
~VizCreations

Reach us at https://www.vizcreations.com

